import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
    firstFighter = { ...firstFighter };
    secondFighter = { ...secondFighter };
    const initialHealth = {
      firstFighter: firstFighter.health,
      secondFighter: secondFighter.health
    };

    return new Promise((resolve) => {
      let pressed = new Set();
      document.addEventListener('keyup', function(event) {
        pressed.delete(event.code);
      });

    document.addEventListener('keydown', function(event) {
      let winner;
      pressed.add(event.code);

      if (pressed.has('KeyA') && !event.repeat && pressed.has('KeyL')) {
        const leftFighterView = document.getElementsByClassName('arena___left-fighter')[0];
        startAnimationAttack(leftFighterView, 'left');
      } else if (pressed.has('KeyA') && !event.repeat && !pressed.has('KeyL')) {
        winner = attackOpponent(firstFighter, secondFighter, 'left', initialHealth.secondFighter);
      }

      if(pressed.has('KeyJ') && !event.repeat && pressed.has('KeyD')) {
        const rightFighterView = document.getElementsByClassName('arena___right-fighter')[0];
        startAnimationAttack(rightFighterView, 'right');
      } else if(pressed.has('KeyJ') && !event.repeat && !pressed.has('KeyD')) {
        winner = attackOpponent(secondFighter, firstFighter, 'right', initialHealth.firstFighter);
      }

      if(winner) resolve(winner); // resolve the promise with the winner when fight is over
    });
  });
}

export function getDamage(attacker, defender) {
  const result = getHitPower(attacker) - getBlockPower(defender);
  return result <= 0 ? 0 : result; 
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.floor(Math.random() * 2) + 1 
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.floor(Math.random() * 2) + 1  
  return fighter.defense * dodgeChance;
}

function attackOpponent(attacker, oponent, position, initialHealth) {
  const oponentIsDead = false;
  const oponentPosition = position === 'right' ? 'left' : 'right';
  const fighterView = document.getElementsByClassName(`arena___${position}-fighter`)[0];
  const healthIndicator = document.getElementById(`${oponentPosition}-fighter-indicator`);
  const remainingHealth = calculateHealthInPersent(oponent, initialHealth, getDamage(attacker, oponent));
  startAnimationAttack(fighterView, position);
  healthIndicator.style.width = `${remainingHealth}%`;
  return remainingHealth ? false : attacker;
}


function calculateHealthInPersent(oponent, initialHealth, damage) {
  const remainingHealth = oponent.health = oponent.health - damage;
  if(remainingHealth <= 0) return 0;
  return (100 * remainingHealth) / initialHealth;
}

function startAnimationAttack(fighterView, position) {
  fighterView.classList.add(`attack-animation-${position}`);
  fighterView.style.animation = 'none';
  fighterView.offsetHeight; // do reflow
  fighterView.style.animation = null;
}