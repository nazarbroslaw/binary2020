import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter ?? false) {
    const fighterImage = createFighterImage(fighter);
    const container = createElement({
      tagName: 'div',
      className: 'fighter-preview___container'
    });

    const blockDetailInfo = createBlockDetailedFigterInfo(fighter);

    container.append(fighterImage, blockDetailInfo);
    fighterElement.append(container);
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}


function createBlockDetailedFigterInfo({name, health, attack, defense}) {
  const detailInfo = createElement({
    tagName: 'div',
    className: 'fighter-preview___detail-info-block detail-info-block'
  });
  detailInfo.innerHTML = `
    <h3 class="detail-info-block__fighter-name">${name}</h3>
    <div class="detail-info-block__fighter-features">
      <div class="detail-info-block__item">
        <div class="detail-info-block__item-feature-name">Health</div>
        <div class="detail-info-block__item-feature-value">${health}</div>
      </div>
      <div class="detail-info-block__item">
        <div class="detail-info-block__item-feature-name">Attack</div>
        <div class="detail-info-block__item-feature-value">${attack}</div>
      </div>
      <div class="detail-info-block__item">
        <div class="detail-info-block__item-feature-name">Defence</div>
        <div class="detail-info-block__item-feature-value">${defense}</div>
      </div>
    </div>
    </div>
  `;
  return detailInfo;
}