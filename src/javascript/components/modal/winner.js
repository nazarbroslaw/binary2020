import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const modalBody = createElement({ tagName: 'div' });
  const newGameButton = createElement({
    tagName: 'button',
    className: 'modal-button-new-game'
  });
  const modalContent = createElement({
    tagName: 'div',
    className: 'modal-content'
  });
  modalContent.innerHTML = `${fighter.name} have won !!!`;
  newGameButton.innerHTML = 'new game';
  newGameButton.addEventListener('click', () => document.location.reload(true));
  modalBody.append(modalContent, newGameButton);
  showModal({
    title: 'Congratulations',
    bodyElement: modalBody,
    onClose: () => document.location.reload(true)
  });
}
